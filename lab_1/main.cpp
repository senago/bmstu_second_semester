#include <iostream>

#include "string.hpp"

int main() {
  String a("abc");
  String b("bce");
  std::cout << a + b << std::endl;

  String c("ababaaba");
  String d("aba");
  std::cout << std::endl << "find " << d << " in " << c << std::endl;
  std::cout << c.find(d) << std::endl;

  c.replace('a', 'x');
  std::cout << c << std::endl;

  return 0;
}
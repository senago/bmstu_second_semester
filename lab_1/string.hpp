#ifndef data_HPP_
#define data_HPP_

#include <cstddef>
#include <iostream>
#include <vector>

class String {
 public:
  String();
  String(char c);
  String(const char* c);
  String(const String& s);

  String& operator=(const String& s);
  String& operator+=(const String& s);
  bool operator==(const String& s) const;
  char operator[](size_t pos) const { return data_[pos % size_]; }

  friend String operator+(const String& lhs, const String& rhs);
  friend String operator+(const String& lhs, const char* rhs);
  friend String operator+(const String& lhs, char rhs);
  friend String operator+(const char* lhs, const String& rhs);
  friend String operator+(char lhs, const String& rhs);

  friend std::ostream& operator<<(std::ostream& os, const String& s);
  friend std::istream& operator>>(std::istream& is, String& s);

  void replace(char from, char to);

  int find(const String& s) const;

  size_t size() const { return size_; }

  ~String() {
    if (data_ != nullptr) {
      delete[] data_;
    }
  }

 private:
  size_t size_ = 0;
  char* data_ = nullptr;
};

#endif  // STRING_HPP_
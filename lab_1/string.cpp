#include "string.hpp"

String::String(){};

String::String(char c) : size_(1) { data_ = new char(c); }

String::String(const char* c) {
  if (c != nullptr) {
    size_t n = 0;
    while (c[n] != '\0') {
      ++n;
    }
    size_ = n;
    data_ = new char[n];
    for (size_t i = 0; i < n; ++i) {
      data_[i] = c[i];
    }
  }
}

String::String(const String& s) {
  size_ = s.size();
  data_ = new char[size_];
  for (size_t i = 0; i < size_; ++i) {
    data_[i] = s[i];
  }
}

String& String::operator=(const String& s) {
  if (this != &s) {
    delete[] data_;

    size_ = s.size();
    data_ = new char[size_];
    for (size_t i = 0; i < size_; ++i) {
      data_[i] = s[i];
    }
  }
  return *this;
}

String& String::operator+=(const String& s) {
  size_t size = size_ + s.size();
  char* str = new char[size];

  for (size_t i = 0; i < size_; ++i) {
    str[i] = data_[i];
  }
  for (size_t i = 0; i < s.size(); ++i) {
    str[size_ + i] = s[i];
  }

  delete[] data_;

  size_ = size;
  data_ = str;
  return *this;
}

bool String::operator==(const String& s) const {
  if (this == &s) {
    return true;
  }

  if (size_ != s.size()) {
    return false;
  }

  for (size_t i = 0; i < size_; ++i) {
    if (data_[i] != s[i]) {
      return false;
    }
  }

  return true;
}

String operator+(const String& lhs, const String& rhs) { return String(lhs) += rhs; }

String operator+(const String& lhs, const char* rhs) { return String(lhs) += String(rhs); }

String operator+(const String& lhs, char rhs) { return String(lhs) += String(rhs); }

String operator+(const char* lhs, const String& rhs) { return String(lhs) += rhs; }

String operator+(char lhs, const String& rhs) { return String(lhs) += rhs; }

int String::find(const String& s) const {
  size_t m = s.size();
  size_t size = size_ + m + 1;
  String r = String(s + '#' + *this);

  // KnuthMorrisPratt
  size_t k = 0;
  std::vector<size_t> p(size);
  p[0] = k;
  for (size_t i = 1; i < size; ++i) {
    while (k > 0 && r[i] != r[k]) {
      k = p[k - 1];
    }
    if (r[i] == r[k]) {
      ++k;
    }
    if (k == m) {
      return i - 2 * m + 1;
    }
    p[i] = k;
  }

  return -1;
}

void String::replace(char from, char to) {
  for (size_t i = 0; i < size_; ++i) {
    if (data_[i] == from) {
      data_[i] = to;
    }
  }
}

std::ostream& operator<<(std::ostream& os, const String& s) {
  if (s.size() > 0) {
    for (size_t i = 0; i < s.size(); ++i) {
      os << s[i];
    }
  } else {
    os << "";
  }

  return os;
}

std::istream& operator>>(std::istream& is, String& s) {
  char* c = new char[1024];
  is >> c;
  s = String(c);
  delete[] c;

  return is;
}

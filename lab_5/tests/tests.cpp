#include <gtest/gtest.h>

#include <algorithm>
#include <numeric>
#include <string>
#include <wector.hpp>

TEST(Wector, init_size_capacity) {
  Wector<int> v(10);
  EXPECT_EQ(v.size(), 10);
  EXPECT_EQ(v.capacity(), 11);
  for (size_t i = 0; i < v.size(); ++i) {
    EXPECT_EQ(v[i], 0);
  }

  v = Wector<int>(3, 9);
  EXPECT_EQ(v.size(), 3);
  EXPECT_EQ(v.capacity(), 4);
  for (size_t i = 0; i < v.size(); ++i) {
    EXPECT_EQ(v[i], 9);
  }

  Wector<int> vv(v);
  EXPECT_EQ(v.size(), 3);
  EXPECT_EQ(v.capacity(), 4);
  for (size_t i = 0; i < v.size(); ++i) {
    EXPECT_EQ(v[i], 9);
  }
}

TEST(Wector, at) {
  Wector<double> v(10);
  for (size_t i = 0; i < v.size(); ++i) {
    v[i] = i;
    EXPECT_EQ(v.at(i), i);
  }
  EXPECT_THROW(v.at(v.size()), std::out_of_range);
}

TEST(Wector, brackets) {
  Wector<int> v(10);
  for (size_t i = 0; i < v.size(); ++i) {
    v[i] = i + 1;
    EXPECT_EQ(v[i], i + 1);
  }
}

TEST(Wector, back_front_pop_back) {
  Wector<double> v;
  v.push_back(1);
  for (size_t i = 0; i < 10; ++i) {
    v.push_back(v[i] * 2.);
  }
  EXPECT_EQ(v.back(), 1024.);
  EXPECT_EQ(v.front(), 1.);

  v.pop_back();
  EXPECT_EQ(v.back(), 512.);
}

TEST(Wector, data) {
  Wector<char> v;
  v.push_back('h');
  v.push_back('e');
  v.push_back('l');
  v.push_back('l');
  v.push_back('o');
  EXPECT_EQ(v.data(), std::string("hello"));
}

TEST(Wector, begin_end_iterator) {
  Wector<int> v;
  for (size_t i = 1; i <= 10; ++i) v.push_back(i);
  EXPECT_EQ(std::accumulate(v.begin(), v.end(), 0), 11 * 5);
  EXPECT_EQ(std::count_if(v.begin(), v.end(), [](auto a) { return a % 2; }), 5);
}

TEST(Wector, empty_clear_resize_reserve) {
  Wector<int> v;
  for (size_t i = 1; i <= 10; ++i) v.push_back(i);
  EXPECT_EQ(v.empty(), false);

  auto t = v.size();
  v.resize(v.size() * 2);
  EXPECT_EQ(v.size(), t);

  v.resize(t / 2);
  EXPECT_EQ(v.size(), t / 2);

  v.resize(0);
  EXPECT_EQ(v.empty(), true);
  EXPECT_EQ(v.capacity() != 0, true);

  v.clear();
  EXPECT_EQ(v.empty(), true);
  EXPECT_EQ(v.capacity() == 1, true);

  v.reserve(t * 2);
  EXPECT_EQ(v.empty(), true);
  EXPECT_EQ(v.capacity(), t * 2);
}

TEST(Wector, insert) {
  Wector<int> v;
  for (size_t i = 1; i <= 4; ++i) v.push_back(i);  // 1, 2, 3, 4

  v.insert(v.begin(), 1);      // 1, 1, 2, 3, 4
  v.insert(v.begin() + 2, 2);  // 1, 1, 2, 2, 3, 4
  v.insert(v.begin() + 4, 3);  // 1, 1, 2, 2, 3, 3, 4
  v.insert(v.begin() + 6, 4);  // 1, 1, 2, 2, 3, 3, 4, 4

  for (size_t i = 1; i < v.size(); i += 2) {
    EXPECT_EQ(v[i], v[i - 1]);
    EXPECT_EQ(v[i], (i + 1) / 2);
  }

  v.insert(v.end(), 2, 5);  // 1, 1, 2, 2, 3, 3, 4, 4, 5, 5
  for (size_t i = 1; i < v.size(); i += 2) {
    EXPECT_EQ(v[i], v[i - 1]);
    EXPECT_EQ(v[i], (i + 1) / 2);
  }

  v.clear();
  v.insert(v.begin(), 10, 1);
  EXPECT_EQ(std::accumulate(v.begin(), v.end(), 0), 10);
  v.insert(v.end(), 10, 2);
  EXPECT_EQ(std::accumulate(v.begin(), v.end(), 0), 30);

  v.clear();
  for (size_t i = 1; i <= 5; ++i) v.push_back(i);  // 1, 2, 3, 4, 5
  size_t n = v.size();
  v.insert(v.end(), v.begin(), v.end());
  for (size_t i = 0; i < n; ++i) {
    EXPECT_EQ(v[i], v[i + n]);
  }
}


TEST(Wector, erase) {
  Wector<int> a;
  size_t n = 5;
  for (size_t i = 1; i <= n; ++i) {
    a.push_back(i);          // 1, 2, 3, 4, 5
  }
  a.erase(a.begin());
  EXPECT_EQ(a.front(), 2);
  EXPECT_EQ(a.size(), n - 1);

  a.erase(a.begin(), a.end() - 1);
  EXPECT_EQ(a.front(), a.back());
  EXPECT_EQ(a.back(), 5);
  EXPECT_EQ(a.size(), 1);
}

TEST(Wector, swap) {
  Wector<int> a;
  Wector<int> b;
  size_t n = 5;
  for (size_t i = 1; i <= n; ++i) {
    a.push_back(i);          // 1, 2, 3, 4, 5
    b.push_back(n - i + 1);  // 5, 4, 3, 2, 1
  }
  a.swap(b);
  for (size_t i = 0; i < n; ++i) {
    EXPECT_EQ(a[i], n - i); // 5, 4, 3, 2, 1
    EXPECT_EQ(b[i], i + 1); // 1, 2, 3, 4, 5
  }

  Wector<int> c(10, 10);
  c.swap(a);
  for (auto x : a) {
    EXPECT_EQ(x, 10);
  }
}

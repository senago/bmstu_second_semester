#ifndef WECTOR_HPP_
#define WECTOR_HPP_

#include <iterator>
#include <stdexcept>

template <typename T>
class WIterator {
 public:
  using pointer = T*;
  using reference = T&;
  using value_type = T;
  using difference_type = std::ptrdiff_t;
  using iterator_category = std::random_access_iterator_tag;

  WIterator(pointer ptr) noexcept : ptr_(ptr) {}

  bool operator==(const WIterator& other) const noexcept { return ptr_ == other.ptr_; };
  bool operator!=(const WIterator& other) const noexcept { return ptr_ != other.ptr_; };

  pointer operator->() noexcept { return ptr_; }
  reference operator*() const noexcept { return *ptr_; }

  WIterator operator+(const difference_type& diff) const noexcept { return WIterator(ptr_ + diff); }
  WIterator operator-(const difference_type& diff) const noexcept { return WIterator(ptr_ - diff); }

  difference_type operator-(const WIterator& other) const { return ptr_ - other.ptr_; }
  friend difference_type distance(const WIterator& first, const WIterator& last) { return last - first; }

  bool operator>(const WIterator& other) const noexcept { return ptr_ > other.ptr_; }
  bool operator<(const WIterator& other) const noexcept { return ptr_ < other.ptr_; }
  bool operator>=(const WIterator& other) const noexcept { return ptr_ >= other.ptr_; }
  bool operator<=(const WIterator& other) const noexcept { return ptr_ <= other.ptr_; }

  reference operator[](size_t index) const noexcept { return ptr_[index]; }

  WIterator& operator++() noexcept {
    ++ptr_;
    return *this;
  }

  WIterator operator++(int) noexcept {
    WIterator tmp = *this;
    ++ptr_;
    return tmp;
  }

  WIterator& operator--() noexcept {
    --ptr_;
    return *this;
  }

  WIterator operator--(int) noexcept {
    WIterator tmp = *this;
    --ptr_;
    return tmp;
  }

  WIterator& operator+=(const difference_type& diff) noexcept {
    ptr_ += diff;
    return *this;
  }

  WIterator& operator-=(const difference_type& diff) noexcept {
    ptr_ -= diff;
    return *this;
  }

 private:
  pointer ptr_ = nullptr;
};

template <typename T>
class Wector {
 public:
  using value_type = T;
  using size_type = size_t;
  using iterator = WIterator<T>;
  using const_iterator = const WIterator<T>;

  Wector() noexcept { create_(); }
  Wector(size_type n, const value_type& value = T()) noexcept { create_(n, value); }

  Wector(const Wector& other) noexcept { copy_(other); }
  Wector& operator=(const Wector& other) noexcept {
    if (this != &other) copy_(other);
    return *this;
  }

  Wector(Wector&& other) noexcept { *this = std::move(other); }
  Wector& operator=(Wector&& other) noexcept {
    if (this != &other) {
      destroy_();
      cap_ = other.cap_;
      size_ = other.size_;
      arr_ = other.arr_;
      other.cap_ = 0;
      other.size_ = 0;
      other.arr_ = nullptr;
    }
    return *this;
  }

  ~Wector() noexcept { destroy_(); }

  value_type& at(size_type pos) {
    if (pos >= size_) throw std::out_of_range("out of range");
    return arr_[pos];
  }

  const value_type& at(size_type pos) const {
    if (pos >= size_) throw std::out_of_range("out of range");
    return arr_[pos];
  }

  value_type& operator[](size_type pos) noexcept { return arr_[pos]; }
  const value_type& operator[](size_type pos) const noexcept { return arr_[pos]; }

  value_type& front() noexcept { return arr_[0]; }
  const value_type& front() const noexcept { return arr_[0]; }

  value_type& back() noexcept { return arr_[size_ - 1]; }
  const value_type& back() const noexcept { return arr_[size_ - 1]; }

  size_type size() const noexcept { return size_; }
  size_type capacity() const noexcept { return cap_; }

  value_type* data() noexcept { return arr_; }
  const value_type* data() const noexcept { return arr_; }

  iterator begin() noexcept { return iterator(&arr_[0]); }
  const_iterator begin() const noexcept { return iterator(&arr_[0]); }

  iterator end() noexcept { return iterator(&arr_[size_]); }
  const_iterator end() const { return iterator(&arr_[size_]); }

  bool empty() const noexcept { return size_ == 0; }
  void clear() noexcept {
    destroy_();
    create_();
  }

  void swap(Wector&& other) noexcept {
    auto tmp = std::move(*this);
    *this = std::move(other);
    other = std::move(tmp);
  }

  void swap(Wector& other) noexcept {
    auto tmp = std::move(*this);
    *this = std::move(other);
    other = std::move(tmp);
  }

  void pop_back() {
    if (size_ == 1) {
      clear();
    } else {
      --size_;
    }
  }

  void push_back(const value_type& value) {
    if (size_ + 1 == cap_) reserve(cap_ * exp_factor_);
    arr_[size_] = value;
    ++size_;
  }

  void reserve(size_type new_cap) {
    if (cap_ >= new_cap) return;
    cap_ = new_cap;

    value_type* tmp = new value_type[cap_];
    for (size_t i = 0; i < size_; ++i) {
      tmp[i] = arr_[i];
    }

    destroy_();
    arr_ = tmp;
  }

  void resize(size_type count, const value_type& value = T()) {
    if (count == size_) return;
    size_type max_right = (cap_ > count ? cap_ : count);
    value_type* tmp = new value_type[max_right];
    size_type min_left = (size_ < count ? size_ : count);

    for (size_type i = 0; i < min_left; ++i) tmp[i] = arr_[i];
    for (size_type i = min_left; i < count; ++i) tmp[i] = value;

    destroy_();
    arr_ = tmp;
    cap_ = max_right;
    size_ = min_left;
  }

  iterator insert(const_iterator pos, value_type&& value) {
    size_type n = size_ + 1;
    value_type* tmp = new value_type[n];

    size_type i = 0;
    auto it = begin();
    while (it != pos) tmp[i++] = *(it++);
    auto head = it;
    tmp[i++] = std::move(value);
    while (it != end()) tmp[i++] = *(it++);

    if (n >= cap_) reserve(n * exp_factor_);
    std::copy(tmp, tmp + n, begin());
    size_ = n;

    return head;
  }

  iterator insert(const_iterator pos, size_type count, const value_type& value) {
    size_type n = size_ + count;
    value_type* tmp = new value_type[n];

    size_type i = 0;
    auto it = begin();
    while (it != pos) tmp[i++] = *(it++);
    auto head = it;
    while (count--) tmp[i++] = value;
    while (it != end()) tmp[i++] = *(it++);

    if (n >= cap_) reserve(n * exp_factor_);
    std::copy(tmp, tmp + n, begin());
    size_ = n;

    return head;
  }

  iterator insert(const_iterator pos, iterator first, const_iterator last) {
    size_type n = size_ + distance(first, last);
    value_type* tmp = new value_type[n];

    size_type i = 0;
    auto it = begin();
    while (it != pos) tmp[i++] = *(it++);
    auto head = it;
    while (first != last) tmp[i++] = *(first++);
    while (it != end()) tmp[i++] = *(it++);

    if (n >= cap_) reserve(n * exp_factor_);
    std::copy(tmp, tmp + n, begin());
    size_ = n;

    return head;
  }

  iterator erase(const_iterator pos) {
    size_type n = size_ - 1;
    value_type* tmp = new value_type[n];

    size_type i = 0;
    auto it = begin();
    while (it != pos) tmp[i++] = *(it++);
    auto head = it++;
    while (it != end()) tmp[i++] = *(it++);

    std::copy(tmp, tmp + n, begin());
    size_ = n;

    return head;
  }

  iterator erase(const_iterator first, const_iterator last) {
    size_type d = distance(first, last);
    size_type n = size_ - d;
    value_type* tmp = new value_type[n];

    size_type i = 0;
    auto it = begin();
    while (it != first) tmp[i++] = *(it++);
    auto head = it;

    it += d;
    while (it != end()) tmp[i++] = *(it++);

    std::copy(tmp, tmp + n, begin());
    size_ = n;

    return head;
  }

 private:
  void destroy_() noexcept { delete[] arr_; }

  void create_() noexcept {
    cap_ = 1;
    size_ = 0;
    arr_ = new value_type[1];
  }

  void create_(size_type n, const value_type& value) noexcept {
    cap_ = n + 1;
    size_ = n;
    arr_ = new T[cap_];
    for (size_type i = 0; i < size_; ++i) {
      arr_[i] = value;
    }
  }

  void copy_(const Wector& other) noexcept {
    destroy_();
    cap_ = other.cap_;
    size_ = other.size_;
    T* tmp = new T[cap_];
    for (size_type i = 0; i < size_; ++i) {
      tmp[i] = other.arr_[i];
    }
    arr_ = tmp;
  }

  size_type cap_ = 0;
  size_type size_ = 0;
  value_type* arr_ = nullptr;

  static constexpr size_type exp_factor_ = 2;
};

#endif  // WECTOR_HPP_

#ifndef CAPACITOR_HPP_
#define CAPACITOR_HPP_

#include "conductor.hpp"

class Capacitor : virtual public Conductor {
 public:
  Capacitor(double epsilon, double plates_area, double plates_distance)
      : epsilon_(epsilon), plates_area_(plates_area), plates_distance_(plates_distance) {
    ++counter;
  }

  double Capacity() const override { return epsilon_ * Constants::EPSILON * plates_area_ / plates_distance_; }

  std::ostringstream Info() const override {
    std::ostringstream ss;
    ss << "Info about the Capactitor #" << number << std::endl;
    ss << "Capacity: " << Capacity() << std::endl;
    ss << "Epsilon: " << epsilon_ << std::endl;
    ss << "Plates area: " << plates_area_ << std::endl;
    ss << "Plates distance: " << plates_distance_ << std::endl;
    return ss;
  }

  static size_t counter;

 private:
  double epsilon_ = 0;
  double plates_area_ = 0;
  double plates_distance_ = 0;
  const size_t number = counter + 1;
};

size_t Capacitor::counter = 0;

#endif  // CAPACITOR_HPP_
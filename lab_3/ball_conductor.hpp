#ifndef BALL_CONDUCTOR_HPP_
#define BALL_CONDUCTOR_HPP_

#include "conductor.hpp"

class BallConductor : virtual public Conductor {
 public:
  BallConductor(double radius, double epsilon) : radius_(radius), epsilon_(epsilon) { ++counter; }

  double Capacity() const override { return 4 * Constants::PI * epsilon_ * Constants::EPSILON * radius_; }

  std::ostringstream Info() const override {
    std::ostringstream ss;
    ss << "Info about the Ball Conductor #" << number << std::endl;
    ss << "Capacity: " << Capacity() << std::endl;
    ss << "Epsilon: " << epsilon_ << std::endl;
    ss << "Radius: " << radius_ << std::endl;
    return ss;
  }

  static size_t counter;

 private:
  double radius_ = 0;
  double epsilon_ = 0;
  const size_t number = counter + 1;
};

size_t BallConductor::counter = 0;

#endif  // BALL_CONDUCTOR_HPP_
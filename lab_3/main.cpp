#include <iostream>
#include <vector>

#include "conductor.hpp"
#include "capacitor.hpp"
#include "ball_conductor.hpp"

int main() {
  std::vector<Conductor*> conductors = {
      new BallConductor(1, 2),     new BallConductor(0.25, 3), new BallConductor(3, 0.5),
      new Capacitor(2, 0.5, 1e-3), new Capacitor(3, 1, 1e-1),  new Capacitor(0.5, 3, 1e-6),
  };

  for (const Conductor* conductor : conductors) {
    std::cout << conductor->Info().str() << std::endl;
    delete conductor;
  }

  return 0;
}
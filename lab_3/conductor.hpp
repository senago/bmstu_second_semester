#ifndef CONDUCTOR_HPP_
#define CONDUCTOR_HPP_

#include <sstream>

#include "constants.hpp"

class Conductor {
 public:
  virtual double Capacity() const = 0;
  virtual std::ostringstream Info() const = 0;
};

#endif  // CONDUCTOR_HPP_
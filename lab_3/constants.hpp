#ifndef CONSTANTS_HPP_
#define CONSTANTS_HPP_

namespace Constants {
const double PI = 3.1415926535;
const double EPSILON = 8.85 * 1e-12;
}  // namespace Constants

#endif  // CONSTANTS_HPP_
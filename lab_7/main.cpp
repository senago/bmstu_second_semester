#include <iomanip>
#include <iostream>
#include <limits>
#include <numeric>
#include <queue>
#include <random>
#include <thread>
#include <vector>

#include "merge_sort.hpp"

#define cast(x) static_cast<int>(x)
#define TIME ((std::clock() - start_time) / CLOCKS_PER_SEC)
std::mt19937 rnd(std::chrono::high_resolution_clock::now().time_since_epoch().count());

double start_time = 0;
double elapsed_time = 0;

template <typename T>
void map_reduce_merge_sort(std::vector<T>& A, size_t nthreads, bool descending = false) {
  using iterator = std::vector<T>::iterator;

  std::thread threads[nthreads];
  iterator positions[nthreads + 1];
  std::vector<T> subarrays[nthreads];

  iterator it = A.begin();
  std::ptrdiff_t stride = A.size() / nthreads;
  for (size_t i = 0; i < nthreads; ++i) {
    positions[i] = it;
    it += stride;
  }
  positions[nthreads] = A.end();

  for (size_t i = 0; i < nthreads; ++i) {
    threads[i] = std::thread([&, i]() {
      subarrays[i] = std::vector<T>(positions[i], positions[i + 1]);
      MergeSort(subarrays[i], 0, positions[i + 1] - positions[i] - 1);
    });
  }

  for (auto& thread : threads) thread.join();

  A.clear();
  for (const auto& subarray : subarrays) {
    A.insert(A.end(), subarray.begin(), subarray.end());
  }
  MergeSort(A, 0, A.size());

  if (descending) std::reverse(A.begin(), A.end());
}

int main() {
  std::vector<int> demo = {2, 4, 1, 5, 17, 19, 21, 3, 8, 4};
  start_time = std::clock();
  map_reduce_merge_sort(demo, 3);
  for (auto x : demo) {
    std::cout << x << ' ';
  }
  std::cout << "Time: " << TIME << std::endl;

  size_t nthreads[] = {1, 2, 3, 4};
  size_t batch_sizes[] = {cast(1e3), cast(1e4), cast(1e5), cast(1e6)};
  for (size_t batch_size : batch_sizes) {
    std::vector<int> A(batch_size);
    for (size_t nt : nthreads) {
      for (size_t i = 0; i < batch_size; ++i) A[i] = rnd();

      start_time = std::clock();
      map_reduce_merge_sort(A, nt);
      elapsed_time = TIME;

      std::cout << "batch_size: " << batch_size << " threads: " << nt;
      std::cout << " time: " << elapsed_time << std::endl;
    }
  }

  return 0;
}
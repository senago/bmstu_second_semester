#ifndef MERGE_SORT_HPP_
#define MERGE_SORT_HPP_

#include <limits>
#include <queue>
#include <vector>

template <typename T>
void Merge(std::vector<T>& A, size_t p, size_t q, size_t r) {
  size_t i = 0;
  std::queue<T> L;
  std::queue<T> R;

  for (i = p; i <= q; ++i) L.push(A[i]);
  for (i = q + 1; i <= r; ++i) R.push(A[i]);

  i = p;
  while (!(L.empty() || R.empty())) {
    if (L.front() <= R.front()) {
      A[i++] = L.front();
      L.pop();
    } else {
      A[i++] = R.front();
      R.pop();
    }
  }

  while (!L.empty()) {
    A[i++] = L.front();
    L.pop();
  }
  while (!R.empty()) {
    A[i++] = R.front();
    R.pop();
  }
}

template <typename T>
void MergeSort(std::vector<T>& A, size_t p, size_t r) {
  if (p >= r) return;
  size_t q = (p + r) / 2;
  MergeSort(A, p, q);
  MergeSort(A, q + 1, r);
  Merge(A, p, q, r);
}

#endif  // MERGE_SORT_HPP_

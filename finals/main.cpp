#include <algorithm>
#include <iostream>
#include <mutex>
#include <sstream>
#include <stdexcept>
#include <string>
#include <thread>
#include <vector>

inline std::string Bold(std::string s) noexcept {
  s = "\033[1m" + s + "\033[22m";
  return s;
}

// ----------------- 1 ----------------- //

bool is_number(const std::string& s) {
  for (char c : s) {
    if (!std::isdigit(c)) return false;
  }
  return true;
}

class InvalidArgument : public std::invalid_argument {
 public:
  InvalidArgument(const std::string& msg, const std::string& name, size_t age) noexcept
      : std::invalid_argument(msg), name_(name), age_(age) {}

  virtual const char* what() const noexcept { return std::invalid_argument::what(); };

  std::string who() const noexcept {
    std::ostringstream stream;
    stream << "Name: " << name_ << " Age: " << age_;
    return stream.str();
  };

  virtual ~InvalidArgument() noexcept {};

 private:
  std::string name_;
  size_t age_ = 0;
};

class Employee {
 public:
  Employee(const std::string& name, size_t age) : name_(name), age_(age) {
    if (age < min_age_ || age > max_age_) {
      throw InvalidArgument("invalid argument: the age is out of bounds", name, age);
    }
  }

  virtual std::ostringstream info() const noexcept {
    std::ostringstream stream;
    stream << "Name: " << name_ << " Age: " << age_;
    return stream;
  }

  virtual void print() const noexcept { std::cout << info().str() << std::endl; }

  const std::string& name() const noexcept { return name_; }
  size_t age() const noexcept { return age_; }

  void name(const std::string& name) { name_ = name; }
  void age(size_t age) { age_ = age; }

 private:
  inline static size_t min_age_ = 18;
  inline static size_t max_age_ = 100;

  std::string name_;
  size_t age_ = 0;
};

std::ostream& operator<<(std::ostream& os, const Employee& s) {
  os << s.info().str();
  return os;
}

std::istream& operator>>(std::istream& is, Employee& s) {
  std::string str;
  while (is >> str) {
    if (is_number(str)) {
      s.age(std::stoi(str));
    } else {
      s.name(str);
    }
  }
  return is;
}

// ----------------- 1 ----------------- //

// ----------------- 2 ----------------- //

class Head : virtual public Employee {
 public:
  Head(const std::string& name, size_t age, const std::string& department)
      : Employee(name, age), department_(department) {}

  virtual std::ostringstream info() const noexcept override {
    std::ostringstream stream = Employee::info();
    stream << " Department: " << department_;
    return stream;
  }

  virtual void print() const noexcept override { std::cout << info().str() << std::endl; }

 private:
  std::string department_;
};

class Heads {
 public:
  void push_back(const Head& head) { heads_.push_back(head); }

  void sort() {
    std::sort(heads_.begin(), heads_.end(), [](const Head& lhs, const Head& rhs) { return lhs.name() < rhs.name(); });
  }

  void print() {
    for (const Head& head : heads_) {
      head.print();
    }
  }

 private:
  std::vector<Head> heads_;
};

// ----------------- 2 ----------------- //

// ----------------- 3 ----------------- //

class Threads {
 public:
  using value_type = std::vector<Employee>;
  
  Threads(const value_type& v1, const value_type& v2) noexcept {
    thread1_ = std::thread(Threads::process, std::cref(v1), 1);
    thread2_ = std::thread(Threads::process, std::cref(v2), 2);
  }

  ~Threads() {
    thread1_.join();
    thread2_.join();
    print_();
  }

  static void process(const value_type& v, int n) {
    if (v.empty()) {
      return;
    }

    float average = 0;
    for (const Employee& e : v) {
      average += e.age();
      std::lock_guard<std::mutex> lg(mutex_);  // mutex is locked
      e.print();                               // mutex is unlocked
    }

    average /= v.size();
    if (n == 1) {
      average1_ = average;
    } else {
      average2_ = average;
    }
  }

 private: 
  void print_() const noexcept {
    std::cout << std::endl << "Average 1: " << average1_ << std::endl;
    std::cout << "Average 2: " << average2_ << std::endl;
  }

  std::thread thread1_;
  std::thread thread2_;

  inline static std::mutex mutex_;
  inline static float average1_ = 0;
  inline static float average2_ = 0;
};

// ----------------- 3 ----------------- //

int main() {
  // ----------------- 1 ----------------- //

  std::cout << Bold("Первое задание") << std::endl;
  Employee e("Mirzoyan Oganes", 18);
  std::cout << e.info().str() << std::endl;

  try {
    Employee ee("Mirzoyan Oganes", 17);
  } catch (const InvalidArgument& ex) {
    std::cout << ex.what() << '\t';
    std::cout << ex.who() << std::endl;
  }

  // ----------------- 1 ----------------- //

  std::cout << std::endl << Bold("Второе задание") << std::endl;
  // ----------------- 2 ----------------- //

  Head h1("Mirzoyan Oganes", 19, "IoT");
  h1.print();  // - статическое связывание

  Employee* eh = &h1;
  eh->print();  // - динамическое связывание

  Head h2("Richard", 20, "E-Commerce");
  Head h3("Bill", 100, "Cross Border Payments");

  Heads heads;
  heads.push_back(h1);
  heads.push_back(h2);
  heads.push_back(h3);

  std::cout << std::endl << "Before sort: " << std::endl;
  heads.print();

  heads.sort();
  std::cout << "After sort: " << std::endl;
  heads.print();

  // ----------------- 2 ----------------- //

  std::cout << std::endl << Bold("Третье задание") << std::endl;
  // ----------------- 3 ----------------- //

  std::vector<Employee> v1 = {h1, h2, h3};

  Head h4("Joseph", 20, "CS");
  Head h5("Donald", 24, "AI");
  Head h6("Isaac", 25, "AR");
  std::vector<Employee> v2 = {h4, h5, h6};

  { Threads threads(v1, v2); }

  // ----------------- 3 ----------------- //

  return 0;
}

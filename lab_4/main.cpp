#include <iostream>
#include <limits>
#include <vector>

#include "RAII.hpp"
#include "cast_from_string.hpp"

constexpr long long MIN = static_cast<long long>(std::numeric_limits<int>::min());
constexpr long long MAX = static_cast<long long>(std::numeric_limits<int>::max());

void OutOfRangeException() {
  std::string s = "abc";
  for (size_t i = 0; i <= s.size(); ++i) {
    try {
      s.at(i) += 1;
    } catch (const std::out_of_range& e) {
      std::cout << "OutOfRange: " << e.what() << std::endl;
    }
  }
}

void LengthErrorException() {
  try {
    std::vector<int> v;
    v.reserve(v.max_size() + 1);
  } catch (const std::length_error& e) {
    std::cout << "LengthError: " << e.what() << std::endl;
  }
}

void BadCastException() {
  try {
    std::runtime_error runtime("a");
    std::logic_error logic("a");
    runtime = dynamic_cast<std::runtime_error&>(logic);
  } catch (const std::bad_cast& e) {
    std::cout << "BadCast: " << e.what() << std::endl;
  }
}

void InvalidArgumentException() {
  try {
    int s = std::stoi("");
  } catch (const std::invalid_argument& e) {
    std::cout << "InvalidArgument: " << e.what() << std::endl;
  }
}

void BadAllocException() {
  try {
    auto n = MAX;
    while (n--) double* v = new double[MAX];
  } catch (const std::exception& e) {
    std::cout << "BadAlloc: " << e.what() << std::endl;
  }
}

void CastExceptionBool() {
  std::string s1 = "";
  std::string s2 = "tRue";

  try {
    FromString<bool>(s1);
  } catch (const InvalidArgument& e) {
    std::cout << e.what() << std::endl;
  }
  try {
    FromString<bool>(s2);
  } catch (const InvalidArgument& e) {
    std::cout << e.what() << std::endl;
  }
}

void CastExceptionInt() {
  std::string s1 = std::to_string(MIN - 1);
  std::string s2 = std::to_string(MAX + 1000);

  try {
    FromString<int>(s1);
  } catch (const Overflow& e) {
    std::cout << e.what() << std::endl;
  }
  try {
    FromString<int>(s2);
  } catch (const Overflow& e) {
    std::cout << e.what() << std::endl;
  }

  s1 = "++20";
  s2 = "-2a";
  std::string s3 = "+";

  try {
    FromString<int>(s1);
  } catch (const InvalidArgument& e) {
    std::cout << e.what() << std::endl;
  }
  try {
    FromString<int>(s2);
  } catch (const InvalidArgument& e) {
    std::cout << e.what() << std::endl;
  }
  try {
    FromString<int>(s3);
  } catch (const InvalidArgument& e) {
    std::cout << e.what() << std::endl;
  }
}

int main() {
  std::cout << "First task" << std::endl;
  OutOfRangeException();
  LengthErrorException();
  InvalidArgumentException();
  BadCastException();
  BadAllocException();

  std::cout << std::endl << "Second task" << std::endl;
  CastExceptionBool();
  CastExceptionInt();

  std::cout << std::endl << "Third task" << std::endl;
  char* leak_address;
  char* no_leak_address;
  {
    Leak leak(10, 'a');
    leak_address = leak.Get();
    NoLeak no_leak(10, 'a');
    no_leak_address = no_leak.Get();
  }
  if (leak_address == std::string(10, 'a')) {
    std::cout << "Leak is found" << std::endl;
  }
  if (no_leak_address != std::string(10, 'a')) {
    std::cout << "No leak" << std::endl;
  }

  return 0;
}

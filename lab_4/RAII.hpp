// Copyright 2021 Oganes Mirzoyan mirzoyan.oganes@mail.ru

#ifndef RAII_HPP_
#define RAII_HPP_

#include <cstddef>

class Leak {
 public:
  Leak(size_t size, char c) {
    s_ = new char[size];
    for (size_t i = 0; i < size; ++i) {
      s_[i] = c;
    }
  }
  char* Get() const { return s_; }
  ~Leak() {}

 private:
  char* s_ = nullptr;
};

class NoLeak {
 public:
  NoLeak(size_t size, char c) {
    s_ = new char[size];
    for (size_t i = 0; i < size; ++i) {
      s_[i] = c;
    }
  }
  char* Get() const { return s_; }
  ~NoLeak() { delete[] s_; }

 private:
  char* s_ = nullptr;
};

#endif  // RAII_HPP_

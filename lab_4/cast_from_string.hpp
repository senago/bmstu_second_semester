// Copyright 2021 Oganes Mirzoyan mirzoyan.oganes@mail.ru

#ifndef CAST_FROM_STRING_HPP_
#define CAST_FROM_STRING_HPP_

#include <stdexcept>  // for std::logic_error
#include <string>
#include <string_view>

inline bool IsDigit(char c) { return (c >= '0') && (c <= '9'); }

namespace StringToBool {
static constexpr std::string_view False_ = "False";
static constexpr std::string_view false_ = "false";
static constexpr std::string_view True_ = "True";
static constexpr std::string_view true_ = "true";
}  // namespace StringToBool

class CastException : public std::logic_error {
 public:
  explicit CastException(const std::string& msg) : std::logic_error(msg) {}
  virtual const char* what() const noexcept { return std::logic_error::what(); }
  virtual ~CastException() noexcept {};
};

class InvalidArgument : public CastException {
 public:
  explicit InvalidArgument(const std::string& msg) : CastException(msg) {}
  virtual const char* what() const noexcept { return CastException::what(); };
  virtual ~InvalidArgument() noexcept {};
};

class Overflow : public CastException {
 public:
  explicit Overflow(const std::string& msg) : CastException(msg) {}
  virtual const char* what() const noexcept { return CastException::what(); };
  virtual ~Overflow() noexcept {};
};

template <typename T>
inline T FromString(const std::string&);

template <>
inline bool FromString(const std::string& s) {
  if (s == StringToBool::False_ || s == StringToBool::false_) {
    return false;
  } else if (s == StringToBool::True_ || s == StringToBool::true_) {
    return true;
  } else {
    throw InvalidArgument("bool FromString: invalid argument");
  }
}

template <>
inline int FromString(const std::string& s) {
  if (s.empty()) throw InvalidArgument("int FromString: string is empty");

  size_t k = 0;
  bool negative = false;
  if (!IsDigit(s[0])) {
    if (s[0] == '-') {
      negative = true;
    } else if (s[0] != '+') {
      throw InvalidArgument("int FromString: invalid argument");
    }
    k = 1;
  }

  if (s.size() < k + 1) {
    throw InvalidArgument("int FromString: no digits");
  }

  int value = 0;
  for (size_t i = k; i < s.size(); ++i) {
    if (!IsDigit(s[i])) {
      throw InvalidArgument("int FromString: invalid argument");
    }
    value = value * 10 + (s[i] - '0');
    if (value < 0) {
      throw Overflow("int FromString: int overflow");
    }
  }

  return (negative ? -value : value);
}

#endif  // CAST_FROM_STRING_HPP_

#ifndef RBTREE_HPP_
#define RBTREE_HPP_

#include <bidirectional_iterator.hpp>
#include <functional>
#include <iostream>
#include <stdexcept>

constexpr bool RED = true;
constexpr bool BLACK = false;

template <typename Key, typename T, typename Compare = std::less<Key>>
class RBTree {
 public:
  using key_type = Key;
  using mapped_type = T;
  using size_type = std::size_t;

  class Node {
   public:
    Node() noexcept {}
    Node(const key_type& key, const mapped_type& data) noexcept
        : p_(nil_), left_(nil_), right_(nil_), key_(key), data_(data) {}

    key_type key() const noexcept { return key_; }
    mapped_type& data() noexcept { return data_; }
    const mapped_type& data() const noexcept { return data_; }
    bool color() const noexcept { return color_; }

    Node* p_ = nullptr;
    Node* left_ = nullptr;
    Node* right_ = nullptr;

    key_type key_;
    mapped_type data_;
    bool color_ = BLACK;
  };

  using value_type = Node*;

  // ------------- ITERATORS ------------- //
  using iterator = BidirectionalIterator<Node*, false>;
  using const_iterator = BidirectionalIterator<const Node*, false>;
  using reverse_iterator = BidirectionalIterator<Node*, true>;
  using const_reverse_iterator = BidirectionalIterator<const Node*, true>;

  iterator begin() noexcept {
    update_edges();
    return iterator(leftmost_, leftmost_, rightmost_, nil_);
  }

  const_iterator begin() const noexcept {
    update_edges();
    return const_iterator(leftmost_, leftmost_, rightmost_, nil_);
  }

  const_iterator cbegin() const noexcept { return begin(); }

  iterator end() noexcept {
    update_edges();
    return iterator(rightmost_, leftmost_, rightmost_, nil_);
  }

  const_iterator end() const noexcept {
    update_edges();
    return const_iterator(rightmost_, leftmost_, rightmost_, nil_);
  }

  const_iterator cend() const noexcept { return end(); }

  reverse_iterator rbegin() noexcept {
    update_edges();
    return reverse_iterator(rightmost_, rightmost_, leftmost_, nil_);
  }

  const_reverse_iterator rbegin() const noexcept {
    update_edges();
    return const_reverse_iterator(rightmost_, rightmost_, leftmost_, nil_);
  }

  const_reverse_iterator crbegin() const noexcept { return begin(); }

  reverse_iterator rend() noexcept {
    update_edges();
    return reverse_iterator(leftmost_, rightmost_, leftmost_, nil_);
  }

  const_reverse_iterator rend() const noexcept {
    update_edges();
    return const_reverse_iterator(leftmost_, rightmost_, leftmost_, nil_);
  }
  const_reverse_iterator crend() const noexcept { return end(); }

  void update_edges() noexcept {
    leftmost_ = trickle_left();
    rightmost_ = trickle_right();
  }

  value_type trickle_left() const noexcept {
    value_type node = root_;
    while (node->left_ != nil_) {
      node = node->left_;
    }
    return node;
  }

  value_type trickle_right() const noexcept {
    value_type node = root_;
    while (node->right_ != nil_) {
      node = node->right_;
    }
    return node;
  }
  // ------------- ITERATORS ------------- //

  RBTree() noexcept {}
  ~RBTree() noexcept {}

  // LeftRotate: performs left rotation
  void LeftRotate(value_type x) {
    if (x->right_ == nil_) {
      throw std::invalid_argument("NIL right child while left rotation");
    }

    value_type y = x->right_;
    x->right_ = y->left_;
    if (y->left_ != nil_) y->left_->p_ = x;
    y->p_ = x->p_;

    if (x->p_ == nil_)
      root_ = y;
    else if (x == x->p_->left_)
      x->p_->left_ = y;
    else
      x->p_->right_ = y;

    x->p_ = y;
    y->left_ = x;
  }

  // RightRotate: performs right rotation
  void RightRotate(value_type x) {
    if (x->left_ == nil_) {
      throw std::invalid_argument("NIL left child while right rotation");
    }

    value_type y = x->left_;
    x->left_ = y->right_;
    if (y->right_ != nil_) y->right_->p_ = x;
    y->p_ = x->p_;

    if (x->p_ == nil_)
      root_ = y;
    else if (x == x->p_->left_)
      x->p_->left_ = y;
    else
      x->p_->right_ = y;

    x->p_ = y;
    y->right_ = x;
  }

  // InsertFixup: performs recoloring after insertion
  void InsertFixup(value_type z) {
    while (z->p_->color_ == RED) {
      if (z->p_ == z->p_->p_->left_) {
        value_type y = z->p_->p_->right_;
        if (y->color_ == RED) {
          z->p_->color_ = BLACK;
          y->color_ = BLACK;
          z->p_->p_->color_ = RED;
          z = z->p_->p_;
        } else {
          if (z == z->p_->right_) {
            z = z->p_;
            LeftRotate(z);
          }
          z->p_->color_ = BLACK;
          z->p_->p_->color_ = RED;
          RightRotate(z->p_->p_);
        }
      } else {
        if (z->p_ == z->p_->p_->right_) {
          value_type y = z->p_->p_->left_;
          if (y->color_ == RED) {
            z->p_->color_ = BLACK;
            y->color_ = BLACK;
            z->p_->p_->color_ = RED;
            z = z->p_->p_;
          } else {
            if (z == z->p_->left_) {
              z = z->p_;
              RightRotate(z);
            }
            z->p_->color_ = BLACK;
            z->p_->p_->color_ = RED;
            LeftRotate(z->p_->p_);
          }
        }
      }
    }

    root_->color_ = BLACK;
  }

  // Insert: takes input and calls insertion algorithm
  std::pair<iterator, bool> Insert(const key_type& key, const mapped_type& data) {
    value_type node = find(key);
    if (node == nil_) {
      value_type z = new Node(key, data);
      Insert(z);
      return {iterator(z, leftmost_, rightmost_, nil_), true};
    } else {
      return {iterator(node, leftmost_, rightmost_, nil_), false};
    }
  }

  // Insert: performs insertion of a node and calls fixup
  void Insert(value_type z) {
    value_type x = root_;
    value_type y = nil_;
    while (x != nil_) {
      y = x;
      if (z->key_ < x->key_) {
        x = x->left_;
      } else {
        x = x->right_;
      }
    }

    z->p_ = y;
    if (y == nil_) {
      root_ = z;
    } else if (z->key_ < y->key_) {
      y->left_ = z;
    } else {
      y->right_ = z;
    }

    z->left_ = nil_;
    z->right_ = nil_;
    z->color_ = RED;

    ++size_;
    InsertFixup(z);
  }

  // Minimum: returns node with minimal key
  value_type Minimum(value_type x) const noexcept {
    while (x->left_ != nil_) x = x->left_;
    return x;
  }

  // Maximum: returns node with maximum key
  value_type Maximum(value_type x) const noexcept {
    while (x->right_ != nil_) x = x->right_;
    return x;
  }

  // Transplant: performs transplation of v onto u
  void Transplant(value_type u, value_type v) {
    if (u->p_ == nil_)
      root_ = v;
    else if (u == u->p_->left_)
      u->p_->left_ = v;
    else
      u->p_->right_ = v;
    v->p_ = u->p_;
  }

  // DeleteFixup: performs recoloring after deletion
  void DeleteFixup(value_type x) {
    while (x != root_ && x->color_ == BLACK) {
      if (x == x->p_->left_) {
        value_type w = x->p_->right_;
        if (w->color_ == RED) {
          w->color_ = BLACK;
          x->p_->color_ = RED;
          LeftRotate(x->p_);
          w = x->p_->right_;
        }
        if (w->left_->color_ == BLACK && w->right_->color_ == BLACK) {
          w->color_ = RED;
          x = x->p_;
        } else {
          if (w->right_->color_ == BLACK) {
            w->left_->color_ = BLACK;
            w->color_ = RED;
            RightRotate(w);
            w = x->p_->right_;
          }
          w->color_ = x->p_->color_;
          x->p_->color_ = BLACK;
          w->right_->color_ = BLACK;
          LeftRotate(x->p_);
        }
      } else {
        value_type w = x->p_->left_;
        if (w->color_ == RED) {
          w->color_ = BLACK;
          x->p_->color_ = RED;
          RightRotate(x->p_);
          w = x->p_->left_;
        }
        if (w->right_->color_ == BLACK && w->left_->color_ == BLACK) {
          w->color_ = RED;
          x = x->p_;
        } else {
          if (w->left_->color_ == BLACK) {
            w->right_->color_ = BLACK;
            w->color_ = RED;
            LeftRotate(w);
            w = x->p_->left_;
          }
          w->color_ = x->p_->color_;
          x->p_->color_ = BLACK;
          w->left_->color_ = BLACK;
          RightRotate(x->p_);
        }
      }
      x = root_;
    }
    x->color_ = BLACK;
  }

  // Delete: takes input and calls deletion algorithm
  size_type Delete(const key_type& key) {
    value_type node = find(key);
    if (node == nil_) return 0;
    if (size() == 1) {
      delete root_;
      root_ = nil_;
    } else {
      Delete(node);
    }
    return 1;
  }

  // Delete: performs deletion of a node and calls fixup
  void Delete(value_type z) {
    value_type x;
    value_type y = z;
    bool y_color = y->color_;
    if (z->left_ == nil_) {
      x = z->right_;
      Transplant(z, z->right_);
    } else if (z->right_ == nil_) {
      x = z->left_;
      Transplant(z, z->left_);
    } else {
      y = Minimum(z->right_);
      y_color = y->color_;
      x = y->right_;
      if (y->p_ == z) {
        x->p_ = y;
      } else {
        Transplant(y, y->right_);
        y->right_ = z->right_;
        y->right_->p_ = y;
      }
      Transplant(z, y);
      y->left_ = z->left_;
      y->left_->p_ = y;
      y->color_ = z->color_;
    }

    --size_;
    delete z;
    if (y_color == BLACK) DeleteFixup(x);
  }

  // COMPLEMENTARY STUFF //

  value_type nil() const noexcept { return nil_; }
  value_type root() const noexcept { return root_; }
  size_type size() const noexcept { return size_; }

  value_type find(const key_type& k) const noexcept { return find(root_, k); }

  value_type find(value_type x, const key_type& k) const noexcept {
    if (x == nil_ || k == x->key_) {
      return x;
    }
    if (k < x->key_) {
      return find(x->left_, k);
    } else {
      return find(x->right_, k);
    }
  }

  void clear() noexcept {
    clear_(root_);
    size_ = 0;
    root_ = nil_;
  }
  bool empty() const noexcept { return root_ == nil_; }

 private:
  inline static value_type nil_ = new Node();  // sentinel for easier edge cases

  void clear_(value_type node) noexcept {
    if (node == nil_) return;
    clear_(node->left_);
    clear_(node->right_);
    delete node;
  }

  size_type size_ = 0;
  value_type root_ = nil_;

  value_type leftmost_ = nil_;
  value_type rightmost_ = nil_;
};

#endif  // RBTREE_HPP_

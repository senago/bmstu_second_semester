#ifndef STACK_HPP_
#define STACK_HPP_

#include <stdexcept>

// found out I don't need this one, but let it be here //

template <typename T>
class Stack {
 public:
  class Node {
   public:
    Node() noexcept {}
    explicit Node(const T& data_) noexcept : data(data_) {}
    T data;
    Node* prev = nil_;
    Node* next = nil_;
  };

  Stack() noexcept {}
  ~Stack() noexcept { clear(); }

  void push(const T& data) noexcept {
    Node* new_node = new Node(data);
    if (empty()) {
      head_ = new_node;
      nil_->next = head_;
    } else {
      head_->next = new_node;
      new_node->prev = head_;
      head_ = new_node;
    }
  }

  T pop() {
    if (empty()) {
      throw std::out_of_range("pop(): linked list is over");
    }
    T data = head_->data;
    head_ = head_->prev;
    delete head_->next;
    return data;
  }

  bool empty() const noexcept { return head_ == nil_; }
  void clear() noexcept {
    while (!empty()) {
      pop();
    }
    delete head_;
    head_ = nil_;
  }

 private:
  inline static Node* nil_ = new Node();

  size_t size_ = 0;
  Node* head_ = nil_;
};

#endif  // STACK_HPP_

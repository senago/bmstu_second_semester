#ifndef BIDIRECTIONAL_ITERATOR_HPP_
#define BIDIRECTIONAL_ITERATOR_HPP_

#include <stdexcept>

// https://stackoverflow.com/questions/12684191/implementing-an-iterator-over-binary-or-arbitrary-tree-using-c-11

template <typename T, bool Reverse>
class BidirectionalIterator {
 public:
  using value_type = T;
  using pointer = value_type*;

  BidirectionalIterator(value_type current, value_type begin, value_type end, value_type nil) noexcept
      : current_(current), begin_(begin), end_(end), nil_(nil) {}

  BidirectionalIterator(const BidirectionalIterator& iterator) noexcept
      : BidirectionalIterator(iterator.current_, iterator.begin_, iterator.end_, iterator.nil_) {}

  BidirectionalIterator& operator=(BidirectionalIterator iterator) noexcept {
    *this = BidirectionalIterator(iterator.current_, iterator.begin_, iterator.end_, iterator.nil_);
    return *this;
  }

  void swap(BidirectionalIterator& iterator) {
    std::swap(current_, iterator.current_);
    std::swap(begin_, iterator.begin_);
    std::swap(end_, iterator.end_);
  }

  pointer operator->() { return &current_; }
  value_type operator*() const { return current_; }

  bool operator==(const BidirectionalIterator& other) const { return current_ == other.current_; }
  bool operator!=(const BidirectionalIterator& other) const { return current_ != other.current_; }

  BidirectionalIterator& operator++() {
    if (Reverse) {
      backward();
    } else {
      forward();
    }
    return *this;
  }

  BidirectionalIterator& operator--() {
    if (Reverse) {
      forward();
    } else {
      backward();
    }
    return *this;
  }

  void forward() {
    if (current_ == end_) {
      throw std::runtime_error("iterator: can't increment the iterator");
    }
    if (current_->right_ != nil_) {
      current_ = current_->right_;
      while (current_->left_ != nil_) {
        current_ = current_->left_;
      }
    } else {
      value_type y = current_->p_;
      while (y->right_ == current_) {
        current_ = y;
        y = y->p_;
      }
      current_ = y;
    }
  }

  void backward() {
    if (current_ == end_) {
      throw std::runtime_error("iterator: can't decrement the iterator");
    }
    if (current_->left_ != nil_) {
      current_ = current_->left_;
      while (current_->right_ != nil_) {
        current_ = current_->right_;
      }
    } else {
      value_type y = current_->p_;
      while (y->left_ == current_) {
        current_ = y;
        y = y->p_;
      }
      current_ = y;
    }
  }

  BidirectionalIterator operator++(int) {
    BidirectionalIterator past = *this;
    ++(*this);
    return past;
  }

  BidirectionalIterator operator--(int) {
    BidirectionalIterator past = *this;
    --(*this);
    return past;
  }

 private:
  value_type current_;
  value_type begin_;
  value_type end_;
  value_type nil_;
};

template <typename T, bool Reverse>
void swap(BidirectionalIterator<T, Reverse>& it1, BidirectionalIterator<T, Reverse>& it2) {
  it1.swap(it2);
}

#endif  // BIDIRECTIONAL_ITERATOR_HPP_

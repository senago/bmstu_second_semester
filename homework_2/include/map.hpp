#ifndef MAP_HPP_
#define MAP_HPP_

#include <functional>
#include <rb_tree.hpp>

template <typename Key, typename T, typename Compare = std::less<Key>>
class Map {
 public:
  using Tree = RBTree<Key, T, Compare>;

  using key_type = Key;
  using mapped_type = T;
  using key_compare = Compare;
  using value_type = typename Tree::Node*;
  using reference = value_type&;
  using size_type = std::size_t;
  using const_reference = const value_type&;

  using iterator = typename Tree::iterator;
  using const_iterator = typename Tree::const_iterator;
  using reverse_iterator = typename Tree::reverse_iterator;
  using const_reverse_iterator = typename Tree::const_reverse_iterator;

  Map() noexcept {}
  Map(const Map& other) noexcept {}

  Map& operator=(const Map& other) noexcept;

  T& at(const key_type& key) {
    value_type it = tree_.find(key);
    if (it == tree_.end()) {
      throw std::out_of_range("map operator[] out of range");
    }
    return (*it)->data();
  }

  const T& at(const key_type& key) const {
    value_type it = tree_.find(key);
    if (it == tree_.end()) {
      throw std::out_of_range("map operator[] out of range");
    }
    return (*it)->data();
  }

  mapped_type& operator[](const Key& key) {
    value_type node = tree_.find(key);
    if (node == tree_.nil()) {
      auto pair = tree_.Insert(key, mapped_type());
      node = *pair.first;
    }
    return node->data();
  }

  iterator begin() noexcept { return tree_.begin(); }
  iterator begin() const noexcept { return tree_.begin(); }
  iterator cbegin() const noexcept { return tree_.cbegin(); }

  iterator end() noexcept { return tree_.end(); }
  iterator end() const noexcept { return tree_.end(); }
  iterator cend() const noexcept { return tree_.cend(); }

  reverse_iterator rbegin() noexcept { return tree_.rbegin(); }
  const_reverse_iterator rbegin() const noexcept { return tree_.rbegin(); }
  const_reverse_iterator crbegin() const noexcept { return tree_.crbegin(); }

  reverse_iterator rend() noexcept { return tree_.rend(); }
  const_reverse_iterator rend() const noexcept { return tree_.rend(); }
  const_reverse_iterator crend() const noexcept { return tree_.crend(); }

  bool empty() const noexcept { return tree_.empty(); }
  size_type size() const noexcept { return tree_.size(); }

  void clear() noexcept {
    tree_.clear();
  }

  std::pair<iterator, bool> insert(const key_type& key, const mapped_type& data) { return tree_.Insert(key, data); }

  void erase(iterator pos) { tree_.Delete(*pos); }
  void erase(iterator first, iterator last) {
    while (first != last) {
      erase(first);
      ++first;
    }
  }
  size_type erase(const key_type& key) {
    return tree_.Delete(key);
  }

  // TODO: void swap(map& other) noexcept;

  // TODO:
  // node_type extract(iterator position);
  // node_type extract(const key_type& x);

  // TODO: merge

  size_type count(const key_type& key) const { return (tree_.find(key) != tree_.nil()); }

  iterator find(const key_type& key) { return iterator(tree_.find(key)); }
  const_iterator find(const key_type& key) const { return const_iterator(tree_.find(key)); }

 private:
  Tree tree_;
};

#endif

#!/usr/bin/env bash

set -e
export GTEST_COLOR=1
cmake -H. -Bbuild
cmake --build build
cmake --build build --target test -- ARGS="--verbose"

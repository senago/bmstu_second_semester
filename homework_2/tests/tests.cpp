#include <gtest/gtest.h>

#include <map.hpp>
#include <rb_tree.hpp>
#include <stack.hpp>
#include <string>
#include <vector>

// https://www.cs.usfca.edu/~galles/visualization/RedBlack.html

TEST(Stack, INIT) {
  Stack<std::string> stack;
  EXPECT_EQ(stack.empty(), true);
  stack.push("first");
  EXPECT_EQ(stack.empty(), false);
  stack.push("second");
  stack.push("third");
  EXPECT_EQ(stack.empty(), false);
  EXPECT_EQ(stack.pop(), "third");
  EXPECT_EQ(stack.empty(), false);
  EXPECT_EQ(stack.pop(), "second");
  EXPECT_EQ(stack.pop(), "first");
  EXPECT_EQ(stack.empty(), true);
}

TEST(RBTree, INIT) {
  RBTree<char, int> tree;
  EXPECT_EQ(tree.size(), 0);
  EXPECT_EQ(tree.empty(), true);
  EXPECT_EQ(tree.root(), tree.nil());
}

TEST(RBTree, INSERT) {
  {
    RBTree<char, int> tree;
    tree.Insert('c', 1);
    tree.Insert('b', 2);
    tree.Insert('a', 3);
    EXPECT_EQ(tree.size(), 3);
    EXPECT_EQ(tree.find('b'), tree.root());
    EXPECT_EQ(tree.root()->data(), 2);
  }
  {
    RBTree<std::string, int> tree;
    tree.Insert("bb", 1);
    tree.Insert("ba", 2);
    tree.Insert("aa", 3);
    tree.Insert("ab", 4);
    tree.Insert("bc", 5);
    tree.Insert("xyz", 6);
    EXPECT_EQ(tree.size(), 6);
    EXPECT_EQ(tree.find("ba"), tree.root());
    EXPECT_EQ(tree.root()->data(), 2);
    EXPECT_EQ(tree.find("xyz")->color(), true);
  }
}

TEST(RBTree, DELETE) {
  {
    RBTree<std::string, int> tree;
    tree.Insert("bb", 1);
    tree.Insert("ba", 2);
    tree.Insert("aa", 3);
    tree.Insert("ab", 4);
    tree.Insert("bc", 5);
    tree.Insert("xyz", 6);

    tree.Delete("xyz");
    EXPECT_EQ(tree.find("xyz"), tree.nil());

    tree.Delete("bc");
    EXPECT_EQ(tree.find("ba"), tree.root());
    EXPECT_EQ(tree.find("bb")->color(), false);
    EXPECT_EQ(tree.find("aa")->right_, tree.find("ab"));

    tree.Delete("ba");
    EXPECT_EQ(tree.find("ab"), tree.root());
  }
}

TEST(RBTree, TRAVERSE_ITERATORS) {
  {
    RBTree<int, std::string> tree;
    std::vector<std::string> v = {"one", "two", "three", "four", "five"};
    for (size_t i = 0; i < 5; ++i) {
      tree.Insert(i + 1, v[i]);
    }
    auto it = tree.begin();
    auto end = tree.end();
    EXPECT_EQ((*it)->data(), v.front());
    EXPECT_EQ((*end)->data(), v.back());

    size_t i = 0;
    while (it != end) {
      EXPECT_EQ((*it++)->data(), v[i++]);
    }

    auto rit = tree.rbegin();
    auto rend = tree.rend();
    i = 4;
    while (rit != rend) {
      EXPECT_EQ((*rit++)->data(), v[i--]);
    }
  }
}

TEST(RBTree, CLEAR) {
  RBTree<int, std::string> tree;
  std::vector<std::string> v = {"one", "two", "three", "four", "five"};
  for (size_t i = 0; i < 5; ++i) {
    tree.Insert(i + 1, v[i]);
  }
  tree.clear();
  EXPECT_EQ(tree.size(), 0);
  EXPECT_EQ(tree.empty(), true);
  EXPECT_EQ(tree.root(), tree.nil());
}

TEST(Map, INIT) {
  Map<int, std::string> m;
  std::vector<std::string> v = {"one", "two", "three", "four", "five"};
  for (size_t i = 0; i < 5; ++i) {
    m[i] = v[i];
  }
  size_t i = 0;
  for (auto it = m.begin(); it != m.end(); ++it) {
    EXPECT_EQ((*it)->data(), v[i++]);
  }
}

TEST(Map, ERASE) {
  Map<int, std::string> m;
  std::vector<std::string> v = {"one", "two", "three", "four", "five"};
  for (size_t i = 0; i < 5; ++i) {
    m[i] = v[i];
  }
  EXPECT_EQ(m.count(0), 1);
  m.erase(0);
  EXPECT_EQ(m.count(0), 0);
}


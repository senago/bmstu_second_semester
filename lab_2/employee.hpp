#ifndef EMPLOYEE_HPP_
#define EMPLOYEE_HPP_

#include <sstream>
#include <string>

class Employee {
 public:
  Employee(std::string employee_name, double daily_salary = 0, unsigned int spent_days = 0)
      : employee_name_(employee_name), daily_salary_(daily_salary), spent_days_(spent_days) {}
  std::ostringstream GetInfo() const;
  double MonthSalary() const;

 private:
  std::string employee_name_;
  double daily_salary_ = 0;
  unsigned int spent_days_ = 0;
};

#endif  // EMPLOYEE_HPP_
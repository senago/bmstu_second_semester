#include "department.hpp"

std::ostringstream Department::GetInfo() const {
  std::ostringstream ss;
  ss << "Name of the department: " << department_name_ << std::endl;
  ss << "Premium rate: " << premium_rate_ << std::endl;
  return ss;
}

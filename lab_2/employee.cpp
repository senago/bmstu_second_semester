#include "employee.hpp"

#include <sstream>

std::ostringstream Employee::GetInfo() const {
  std::ostringstream ss;
  ss << "Full name: " << employee_name_ << std::endl;
  ss << "Dailry salary: "
     << "💵💲💵" << daily_salary_ << "💵💲💵" << std::endl;
  ss << "Spent days: " << spent_days_ << std::endl;
  return ss;
}

double Employee::MonthSalary() const { return spent_days_ * daily_salary_; }

#include <iostream>

#include "demployee.hpp"

int main() {
  Department department("MIT", 10);
  std::cout << department.GetInfo().str() << std::endl;

  Employee employee("Richard Phillips Feynman", 200);
  std::cout << employee.GetInfo().str() << std::endl;

  Demployee department_employee("Richard Phillips Feynman", "MIT", 200, 10, 10);
  std::cout << department_employee.GetInfo().str() << std::endl;
  return 0;
}
#include "demployee.hpp"

std::ostringstream Demployee::GetInfo() const {
  std::ostringstream ss;
  ss << "The full information about the department employee" << std::endl;
  ss << Employee::GetInfo().str() << Department::GetInfo().str();
  ss << "Overall salary: "
     << "💰" << MonthSalary() << "💰" << std::endl;
  return ss;
}

double Demployee::MonthSalary() const { return Employee::MonthSalary() * Department::premium_rate_; }

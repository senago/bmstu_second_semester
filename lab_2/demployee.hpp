#ifndef DEMPLOYEE_HPP_
#define DEMPLOYEE_HPP_

#include "department.hpp"
#include "employee.hpp"

class Demployee : private Employee, private Department {
 public:
  Demployee(std::string employee_name, std::string department_name, double daily_salary = 0,
            unsigned int spent_days = 0, double premium_rate = 1)
      : Employee(employee_name, daily_salary, spent_days), Department(department_name, premium_rate) {}
  std::ostringstream GetInfo() const;
  double MonthSalary() const;
};

#endif  // DEMPLOYEE_HPP_
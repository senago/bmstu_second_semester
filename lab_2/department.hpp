#ifndef DEPARTMENT_HPP_
#define DEPARTMENT_HPP_

#include <sstream>
#include <string>

class Department {
 public:
  Department(std::string department_name, double premium_rate = 1)
      : department_name_(department_name), premium_rate_(premium_rate){};
  std::ostringstream GetInfo() const;

 private:
  std::string department_name_;

 protected:
  double premium_rate_ = 1;
};

#endif  // DEPARTMENT_HPP_